Rails.application.routes.draw do
  resources :aria
  root to: 'aria#index'
  devise_for :users
  resources :users

  post "aria/upload", :as =>"upload"
end
