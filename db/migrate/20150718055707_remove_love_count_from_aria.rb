class RemoveLoveCountFromAria < ActiveRecord::Migration
  def change
  	remove_column :aria, :love_count
  	add_column :aria, :likes_count, :integer, default: 0
  end
end
