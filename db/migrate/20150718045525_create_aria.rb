class CreateAria < ActiveRecord::Migration
  def change
    create_table :aria do |t|
      t.string :title
      t.string :description
      t.integer :love_count

      t.timestamps null: false
    end
    add_index :aria, :description
    add_index :aria, :love_count
  end
end
