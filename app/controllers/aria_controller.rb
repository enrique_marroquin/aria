class AriaController < ApplicationController
  before_action :set_arium, only: [:show, :edit, :update, :destroy]

  # GET /aria
  # GET /aria.json
layout 'backend'
  def index
    @aria = Arium.all
    s3 = Aws::S3::Resource.new
    bucket = s3.bucket('ariafiles')
    @songs = bucket.objects
  end

  # GET /aria/1
  # GET /aria/1.json
  def show
  end

  # GET /aria/new
  def new
    @arium = Arium.new
  end

  # GET /aria/1/edit
  def edit
  end

  # AWS::S3::S3Object.store(sanitize_filename(params[:mp3file].original_filename), params[:mp3file].read, 
  #   BUCKET, :access => :public_read)
  #     redirect_to root_path

  def upload 
    begin
      s3 = Aws::S3::Resource.new
      bucket = s3.bucket('ariafiles')
      obj = Aws::S3::Object.new('ariafiles', sanitize_filename(params[:mp3file].original_filename))
      obj.upload_file(params[:mp3file].path, acl: 'public-read' )
      # obj.upload_file(sanitize_filename(params[:mp3file].original_filename))
      redirect_to root_path
    # rescue
    #   render :text => "Couldn't complete the upload!"
    end
  end

  # def delete
  #   if (params[:song])
  # end

  # POST /aria
  # POST /aria.json
  def create
    @arium = Arium.new(arium_params)

    respond_to do |format|
      if @arium.save
        format.html { redirect_to @arium, notice: 'Arium was successfully created.' }
        format.json { render :show, status: :created, location: @arium }
      else
        format.html { render :new }
        format.json { render json: @arium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /aria/1
  # PATCH/PUT /aria/1.json
  def update
    respond_to do |format|
      if @arium.update(arium_params)
        format.html { redirect_to @arium, notice: 'Arium was successfully updated.' }
        format.json { render :show, status: :ok, location: @arium }
      else
        format.html { render :edit }
        format.json { render json: @arium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /aria/1
  # DELETE /aria/1.json
  def destroy
    @arium.destroy
    respond_to do |format|
      format.html { redirect_to aria_url, notice: 'Arium was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def sanitize_filename(file_name)
      just_filename = File.basename(file_name)
      just_filename.sub(/[^\w\.\-]/,'_')
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_arium
      @arium = Arium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def arium_params
      params.require(:arium).permit(:title, :description, :love_count, :tag_list, :songs, :bucket)
    end
end
