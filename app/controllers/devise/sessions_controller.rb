class Devise::SessionsController < ApplicationController

  def destroy
    current_user.destroy
    redirect_to root
  end
end
