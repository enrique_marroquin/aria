json.array!(@aria) do |arium|
  json.extract! arium, :id, :title, :description, :likes_count
  json.url arium_url(arium, format: :json)
end
