class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :aria
  has_many :likes
  has_many :liked_aria, through: :likes, source: :arium
  has_many :followers, inverse_of: :user
  has_many :following, inverse_of: :user, class_name: 'Follower',
  										 foreign_key: 'follower_id'
end
