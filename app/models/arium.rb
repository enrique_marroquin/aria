class Arium < ActiveRecord::Base
	
	validates :title, presence: true
	validates :title, length: { minimum:1, maximum: 50 }

	acts_as_ordered_taggable
	acts_as_ordered_taggable_on :genres

	enum arium_type: [:audio, :video]

  belongs_to :user
  has_many :likes
end
