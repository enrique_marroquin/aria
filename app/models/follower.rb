class Follower < ActiveRecord::Base
	belongs_to :user, inverse_of: :followers
	belongs_to :follower, inverse_of: :followers, class_name: 'User',
												foreign_key: 'follower_id'
end
