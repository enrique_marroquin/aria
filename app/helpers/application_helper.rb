module ApplicationHelper
	def download_url_for(song_key)
		s3 = Aws::S3::Resource.new
		s3.bucket('ariafiles').object(song_key).public_url
	end
end
