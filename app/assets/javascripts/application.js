// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require bootstrap-sprockets
//= bootstrap.js
//= app.js
//= slimscroll/jquery.slimscroll.min.js
//= app.plugin.js
//= jPlayer/jquery.jplayer.min.js
//= jPlayer/add-on/jplayer.playlist.min.js
//= jPlayer/demo.js
$(document).ready(function() {
    var audioSection = $('section#audio');
    $('a.html5').click(function() {
 
        var audio = $('<audio>', {
             controls : 'controls'
        });
 
        var url = $(this).attr('href');
        $('<source>').attr('src', url).appendTo(audio);
        audioSection.html(audio);
        return false;
    });
});

//= require_tree .
